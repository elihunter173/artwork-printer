import sys

order = int(sys.argv[1])
triangle = ['▲']
space = ' '

for currentOrder in range(order):

    # makes the triangle a block with spaces
    for i in range(len(triangle)):
        while len(triangle[i]) < 2 ** (currentOrder + 1) - 1:
            triangle[i] = triangle[i] + ' '

    # create copy of array
    triangle_tmp = triangle.copy()

    # adds a sufficient amount of spaces for the leading array
    for i in range(len(triangle)):
        triangle[i] = space + triangle[i]

    # duplicates the array with a space between them
    for i in range(len(triangle_tmp)):
        triangle_tmp[i] = triangle_tmp[i] + ' ' + triangle_tmp[i]

    # combine the two arrays
    triangle = triangle + triangle_tmp

    # double the length of the spaces
    space = space + space

# print the triangle
for line in triangle:
    print(line)
