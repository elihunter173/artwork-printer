# Artwork-Printer

> ASCII Art for Everyone!

## Getting Started

### Installation

Simply clone the repo and then run the artwork you want in the terminal.

```shell
git clone https://gitlab.com/elihunter173/ArtworkPrinter-SierpinskisTriangle.git
cd Artwork-Printer
python SierpinskisTriangle.py
```

## Authors

- **Eli W. Hunter**

## Sierpinski's Triangle

### Order 0
```no-highlight
▲
```

### Order 1
```no-highlight
 ▲
▲ ▲
```

### Order 2
```no-highlight
   ▲
  ▲ ▲
 ▲   ▲
▲ ▲ ▲ ▲
```

### Order 3
```no-highlight
       ▲
      ▲ ▲
     ▲   ▲
    ▲ ▲ ▲ ▲
   ▲       ▲
  ▲ ▲     ▲ ▲
 ▲   ▲   ▲   ▲
▲ ▲ ▲ ▲ ▲ ▲ ▲ ▲
```

### Order ...
